package AssignmentArray;

public class smallarray 
{
		   public static int getSmallest(int[] a) 
		   {
		      int temp;
		      //sort the array
		      for (int i = 0; i < a.length; i++)
		     {
		         for (int j = i + 1; j < a.length; j++) 
		         {
		            if (a[i] > a[j]) 
		            {
		               temp = a[i];
		               a[i] = a[j];
		               a[j] = temp;
		            }
		         }
		      }
		      //return smallest element
		      return a[0];
		   }
		   public static void main(String args[]) 
		   {
		      int a[] = {161,100,444,315,16,213,232};
		      System.out.println("Smallest number is: " + getSmallest(a));
		   }
		}

