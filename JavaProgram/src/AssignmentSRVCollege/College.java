package AssignmentSRVCollege;
import java.util.Scanner;

public class College {

	   public static void main(String[] args)
	{
		int cse,ece,mech; // Try-Catch block to sanitize non-integer input
	try
	{  // Initialize Scanner object
	Scanner sc = new Scanner(System.in); // Take user input
		
	System.out.print("The number of students placed in ComputerScience: ");
		cse = sc.nextInt();
		
		System.out.print("The number of students placed in EectronicCommunication: ");
		ece = sc.nextInt();
		
		System.out.print("The number of students placed in Mechanical: ");
		mech = sc.nextInt();
	  sc.close();
	}
	catch (Exception e)
	{
		System.out.println("Entered value is integer value and it is invalid input.");
		return;
	}
	// If any integer is negative, print message and exit
	
	if (cse < 0 || ece < 0 || mech < 0)
	{
		System.out.println("Entered value is non-negative integer and it is invalid input.");
		return;
	}
	
	// If all values are equal, print message and exit
	if (cse == ece && ece == mech)
	{
		System.out.println("This department has not got the highest placement.");
		return;
	}
		System.out.println("This department has got the highest Placement :");
		
	// First, check if any two values are equal and greater than the third
	if (cse == ece && cse > mech)
	{
		System.out.println("ComputerScience");
		System.out.println("EectronicCommunication");
	}
	
	else if (cse == mech && cse > ece)
	{
		System.out.println("ComputerScience");
		System.out.println("Mechanical");
	}
	
	else if (ece == mech && ece > cse)
	{
		System.out.println("EectronicCommunication");
		System.out.println("Mechanical");
	}
	
	// Now, if we reached here, all values are distinct
	// Check if one value is greater than both
	
	else if (cse > ece && cse > mech)
	{
	System.out.println("ComputerScience");
	}
	
	else if (ece > mech)
	{
	System.out.println("EectronicCommunication");
	}
	
	else
	{
	System.out.println("Mechanical");
	}
  }
}
