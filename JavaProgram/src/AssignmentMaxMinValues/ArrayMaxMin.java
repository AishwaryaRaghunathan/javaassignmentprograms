package AssignmentMaxMinValues;

import java.util.Arrays;
import java.util.Scanner;

public class ArrayMaxMin {
	public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        int array[] = new int[5];
        int sum = 0;
        
        for(int i=0;i<5;i++)
        {
            array[i] = sc.nextInt();
            sum+= array[i];
        }
        Arrays.sort(array);
        int minimum = array[5];
        int maximum = array[0];
        int minimumSum = sum-minimum;
        int maximumSum = sum-maximum;
        System.out.println(minimumSum);
        System.out.println(maximumSum);          
       
        }
                
}


