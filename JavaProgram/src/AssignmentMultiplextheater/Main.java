package AssignmentMultiplextheater;
import java.util.Scanner;
public class Main
{
   public static void main (String[]args)
  {
		int noTicket;
		double total = 0, cost;
		String ref, co, circle;
		Scanner s = new Scanner (System.in);
		System.out.println ("Enter the number of ticket:");
		noTicket = s.nextInt ();
		
		if (noTicket < 5 || noTicket > 40)
		{
		  System.out.println ("The Minimum of 5 tickets and Maximum of 40 tickets");
		  System.exit (0);
		}
		
		System.out.println ("Do you want refreshment:");
		ref = s.next ();
		
		System.out.println ("Do you have coupon code:");
		co = s.next ();
		
		System.out.println ("Enter the circle:");
		circle = s.next ();
		
		if (circle.charAt (0) == 'K')
		cost = 75 * noTicket;
		else if (circle.charAt (0) == 'Q')
		cost = 150 * noTicket;
		else
		{
		  System.out.println ("This is Invalid Input");
		  return;
		}
		total = cost;
		
		if (noTicket > 20)
		cost = cost - ((0.1) * cost);
		total = cost;
		
		if (co.charAt (0) == 'Y')
		total = cost - ((0.02) * cost);
		
		if (ref.charAt (0) == 'Y')
		total += (noTicket * 50);
		
		System.out.format ("The ticket cost is :%.2f", total);
		}
	}
		
