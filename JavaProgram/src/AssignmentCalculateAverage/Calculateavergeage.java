package AssignmentCalculateAverage;
import java.util.*;
import java.text.DecimalFormat;

public class Calculateavergeage
{
 double calculateAverage(int[] age)
    {
        int len=age.length;
        double sum=0.0;
        for(int i=0;i<len;i++)
        {
            sum+=age[i];
        }
        double avg=sum/len;
        return avg;
    }
    public static void main (String[] args)
    {
        Scanner sc =new Scanner(System.in);
        Calculateavergeage obj=new Calculateavergeage();
        System.out.println("Total number of employees:");
        int n=sc.nextInt();
        int flag=0;
        if(n>1)
        { 
        	int[] age=new int[n];
            System.out.println("The age of "+n+" employees:");
            for(int i=0;i<n;i++)
            {
                int temp=sc.nextInt();
                if(temp>=28 && temp<=40)
                {
                    age[i]=temp;
                }
                else
                {
                    System.out.println("This is invalid age");
                    flag++;
                    break;
                }
            }
            if(flag==0)
            {   DecimalFormat df=new DecimalFormat("####.00");
                System.out.println("The average age is "+df.format(obj.calculateAverage(age)));
            }
        }
        else
        {
            System.out.println("Enter a valid employee");
        }
    }
}
