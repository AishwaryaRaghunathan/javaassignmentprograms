package AssignmentArraylist;
import java.util.*;
public class Arraylist 
{
	public static void main(String[] args) {
	   ArrayList<String> arraylist = new ArrayList<String>();
			arraylist.add("aishu");
			arraylist.add("aish");
			arraylist.add("ais");
		System.out.println(arraylist.size());
		System.out.println("While Loop ArrayList:");
		  Iterator<String> itr = arraylist.iterator();
	      while(itr.hasNext()) 
	      {
	           System.out.println(itr.next());
	         }
	           System.out.println("Advanced For Loop ArrayList:");
	       for(Object obj : arraylist) 
	       {
	           System.out.println(obj);
	         }
	           System.out.println("For Loop ArrayList:");
	        for(int i=0; i<arraylist.size(); i++)
	        {
	           System.out.println(arraylist.get(i));
	          }
	      }
	}
